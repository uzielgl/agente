<Ifmodule mod_rewrite.c>
        Options +FollowSymLinks
        #remove index.php from URL
        RewriteEngine on
        RewriteCond $1 !^(info\.php|index\.php|test-import-table\.php|cgi-bin|swf|inc|banners|downloads|plantilla|estilo_general|images|imgbanners|imgcasas|imginmobiliarias|imgpromotores|js|ckfinder|css|chat|shared|vidcasas|xml|robots\.txt|favicon\.ico|themes)
        RewriteRule ^(.*)$ index.php?/$1 [L]
</IfModule>

<IfModule !mod_rewrite.c>
    ErrorDocument 404 /index.php
</IfModule>




<IfModule mod_expires.c>
	ExpiresActive On
	ExpiresDefault A0
	ExpiresByType text/css "access plus 0 seconds"
	ExpiresByType text/javascript "access plus 0 seconds"
	ExpiresByType application/x-javascript "access plus 0 seconds"
	ExpiresByType text/javascript "access plus 0 seconds"
	ExpiresByType application/javascript "access plus 0 seconds"
</IfModule>

<IfModule !mod_expires.c>
    ErrorDocument 404 /index.php
</IfModule>


