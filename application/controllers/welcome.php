<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends MY_Controller {
    
    public function __construct(){
        parent::__construct();
    }

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$this->addJs("mootools-core-1.4.5-full-nocompat.js");
		
		$this->addJs("astar/astar.js");
		$this->addJs("astar/graph.js");
		
		$this->addJs("clases/Ficha.js");
		$this->addJs("clases/Fantasma.js");
		$this->addJs("clases/Objeto.js");
		$this->addJs("clases/Pacman.js");
		$this->addJs("clases/Casilla.js");
		$this->addJs("clases/Coordenada.js");
		$this->addJs("clases/Tablero.js");
		$this->addJs("agente2.js");
		$this->view("agente");
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */