<?php

class MY_Form_validation extends CI_Form_validation{
    
    /**
     * Greather or equal than
     *
     * @access  public
     * @param   string
     * @return  bool
     */
    public function greater_equal_than($str, $min)
    {
        if ( ! is_numeric($str))
        {
            return FALSE;
        }
        return $str >= $min;
    }

    // --------------------------------------------------------------------

    /**
     * Less or equal than
     *
     * @access  public
     * @param   string
     * @return  bool
     */
    public function less_equal_than($str, $max)
    {
        if ( ! is_numeric($str))
        {
            return FALSE;
        }
        return $str <= $max;
    }
    
}
