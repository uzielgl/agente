<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------


/** 
	* Recibe la fecha que viene en d/m/Y y la transforma a formato mysql (Y-m-d)
	* 
*/

/* Para documentos */
if ( ! function_exists('flog'))
{
	function flog($p1, $p2= NULL){
		global $fp;
		$fp->log($p1, $p2);
	}
}

/*Regresa el mensaje en singular o plural pasado de acuerdo a la cantidad*/
if( ! function_exists( "msg_by_qty" ) ){
	function msg_by_qty($singular_message, $plural_message, $quantity = 0){
		return ( $quantity == 1 or $quantity == -1 ) ? $singular_message : $plural_message;
	}
}

/*Convierte una palabra de plural a singular */
if( ! function_exists("to_singular") ){
	function to_singular($word){
		//Posibles casos casas, terrenos
		$word = trim($word);
		if( count ( explode(" ", $word) ) == 1 ){ // Aseguramos que sólo sea una palabra
			$fin = substr($word, strlen( $word ) - 2 );
			if( $fin == "es"){
				$word = substr($word, 0, strlen( $word ) - 2 );
			}else{
				$fin = substr($word, strlen( $word ) - 1 );
				if( $fin == "s" )
					$word = substr($word, 0, strlen( $word ) - 1 );										
			}
		}
		return $word;
	}
}


if( ! function_exists("get_cmb_hour") ){
	function get_cmb_hour($from= 0 , $to=23){
		$hours = array(""=> "hr");
		foreach( range($from, $to) as $i){
			$hour = sprintf("%02d", $i);
			$hours[ $hour ] = $hour;
		}		
		return $hours;
	}
}

if( ! function_exists("get_cmb_minute") ){
	
	function get_cmb_minute($from= 0 , $to=59){
		$minutues = array(""=>"min");
		foreach( range($from, $to) as $i){
			$min = sprintf("%02d", $i);
			$minutues[ $min ] = $min;
		}		
		return $minutues;
	}
}


if( ! function_exists("image_exits") ){
    function image_exits($file){
        return file_exists("./$file");
    }
}

if( ! function_exists("image_casa" ) ){
    function image_casa($img_name, $with_patch = TRUE){ 
        if( $img_name && image_exits( "imgcasas/" . $img_name ) ) 
            return $img_name;
        else
            return "sin-imagen.png";
    }
}



