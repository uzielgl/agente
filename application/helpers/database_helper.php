<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2011, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Form Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/form_helper.html
 */

// ------------------------------------------------------------------------


/** 
	* Recibe la fecha que viene en d/m/Y y la transforma a formato mysql (Y-m-d)
	* 
*/

if ( ! function_exists('date_to_mysql'))
{
	function date_to_mysql($date, $format = "y-m-d" ){
		if (!$date) return "";
		try{
			list($d, $m, $Y) = explode("/", $date);
		}catch (Exception $e){ 
			return "";
		}
		return "$Y-$m-$d";
		
	}
}

/*Se le pasa una cadena de fecha en formato YYYY-MM-DD ó YYYY-MM-DD HH:II:SS y la convierte a DD/MM/YYYY*/
if ( ! function_exists('date_to_human'))
{
	function date_to_human($date, $format = "d-m-Y" ){
		if (!$date) return "";
		$t = explode(" ", $date);
		$date = $t[0];
		
		if( $date == "0000-00-00") return "";
		
		$time = isset( $t[1] ) ? $t[1] : ""; 
		list( $Y, $m, $d ) = explode( "-", $date );
		
		return "$d/$m/$Y";
		
	}
}


/*Convierte una fecha (mdate )de mysql en formato yyyy-mm-dd hh*/
function mdate_to_datepicker($mdate){
	if ( ! $mdate ) return "";
	$t = explode(" ", $mdate);
	return $t[0] == "0000-00-00" ? "" : $t[0];  
}


/*Recupera un array con los valores de $field encontrados*/
function get_field($table, $field, $field_math, $math){
	$CI =& get_instance();
	$CI->load->database();

	$CI->db->select($field);
	$CI->db->from($table);
	$CI->db->where($field_math, $math);
	$rs =  $CI->db->get()->result_array();
	$return = array();
	foreach( $rs as $i ){
		$return[] = $i[ $field ];
	}
	return $return;
}


if( !function_exists("get_cmb_format") ){
	function get_cmb_format($array, $id, $val){
		$array_cmb = array();
		foreach( $array as $i ){
			$array_cmb[ $i[$id] ] = $i[ $val ];
		}
		return $array_cmb;
	}
}

/* Regresa la información para darsela a un combo
	 * */
function get_cmb_data( $table, $id, $value ){
	$CI =& get_instance();
	$CI->load->database();
	return get_cmb_format( $CI->db->get($table)->result_array(), $id, $value );
}