<?php

class MY_Controller extends CI_Controller{
    
    //Variables de control para el tag_title
    var $tag_title = "";
    var $tag_prefix_title = "";
    var $tag_set_prefix_title = TRUE;
    var $tag_show_section_title = TRUE;

	public function __construct($logued_required=TRUE){
		parent::__construct();
		$this->data = array();
        
        if( $this->uri->rsegment(1) !== "admin" ){
            //Elementos css y js generales para el front
            $this->data['js'] = array( 
                "base" => array(
                    "jquery-1.9.1.min.js", "bootstrap/bootstrap.min.js", "modernizr-2.6.2-respond-1.1.0.min.js", "jquery.complete-placeholder.js" ),
                "plugs" => array(),
                "globals" => array("extend.js", "init.js"),
                "page" => array()
            );
            
            $this->data["css"] = array( 
                "base" => array(
                   "bootstrap/bootstrap.min.css", "bootstrap/bootstrap-responsive.min.css"),
                "plugs" => array(),
                "globals" => array("base.css"),
                "page" => array()
            ); 
        }else{
             //Elementos css y js generales para el front
            $this->data['js'] = array( 
                "base" => array(
                    "jquery-1.9.1.min.js", "bootstrap/bootstrap.min.js", "modernizr-2.6.2-respond-1.1.0.min.js", "jquery.complete-placeholder.js" ),
                "plugs" => array(),
                "globals" => array("extend.js", "init.js"),
                "page" => array()
            );
            
            $this->data["css"] = array( 
                "base" => array(
                   "bootstrap/bootstrap.min.css", "bootstrap/bootstrap-responsive.min.css"),
                "plugs" => array(),
                "globals" => array("base.css"),
                "page" => array()
            ); 
        }
        
        $this->data["prefix_title"] = " ";

		$this->data['data'] = array();
		
	}
    
    /** Estable el tag "title" del sitio web
     * @param title El título del sitio web
     * @param prefix Define si se pone o no el prefijo "AMPI " 
     * @param section Defin si se pone o no la sección ej. Guadalajara, Colima, etc
     * @author uzielgl@hotmail.com
     */
    public function setTitle( $title, $prefix = TRUE, $section = TRUE ){
        $this->tag_title = $title;
        $this->tag_set_prefix_title = $prefix;
        $this->tag_show_section_title = $section;
    }
    
    /** Generá el título del sitio web en base a lo establecido en setTitle
     * @return String El título que se pondrá en el sitio
     * @author uzielgl@hotmail.com
     * */
    private function _getTitle(){
        $prefix = $this->tag_set_prefix_title ? $this->tag_prefix_title : "";
        $section = $this->tag_show_section_title ?  "" : "";
        return trim( "$prefix $section {$this->tag_title}" );
    }
    
    
    /** Agrega un archivo JS al sitio web
     * @param $file La ubicación del archivo a partir del tema seleccionado
     * @param $file Establece donde se pondrá el archivo, posibles opciones last|first|N
     * */
     /*
    public function addJs($file, $position="last"){
        if( is_array($file) )
            $this->data["js"] = array_merge( $this->data["js"], $file );
        else
            $this->data["js"][] = $file;
    }*/
    
    public function addJs($file, $section="page", $position="last"){
        if( is_array($file) ){
            if( $position == "last" ){
                $this->data["js"][$section] = array_merge(
                    $this->data["js"][$section], 
                    $file 
                );
            }elseif( $position == "first" ){
                $this->data["js"][$section] = array_merge(
                    $file, 
                    $this->data["js"][$section]
                );
            }
        }else{
            if( $position == "last"){
                $this->data["js"][$section][] = $file;
            }elseif( $position == "first" ){
            }
        }
    }
    
    /** Agrega un archivo CSS al sitio web
     * @param $file La ubicación del archivo a partir del tema seleccionado
     * @param $file Establece donde se pondrá el archivo, posibles opciones last|first|N
     * */
    public function addCss($file, $section="page", $position="last"){
        if( is_array($file) ){
            if( $position == "last" ){
                $this->data["css"][$section] = array_merge(
                    $this->data["css"][$section], 
                    $file 
                );
            }elseif( $position == "first" ){
                $this->data["css"][$section] = array_merge(
                    $file, 
                    $this->data["js"][$section]
                );
            }
        }else{
            if( $position == "last"){
                $this->data["css"][$section][] = $file;
            }elseif( $position == "first" ){
            }
        }
    }


	public function sresponse($p1=null, $p2=null) { //Success response
		$r = array('success' => TRUE);
		if ($p1 and $p2){ //means that p1 is msg and p2 is extra data
			$r['msg'] = $p1;
			$r= array_merge($r, $p2);
		}elseif($p1 and !$p2)//means that only there are one parameter
			if(is_string($p1)) // If is a string set in msg
				$r['msg'] = $p1;
			elseif(is_array($p1)) // if is a array merge
				$r = array_merge($r, $p1);
		return json_encode($r);

	}

	public function fresponse($p1=null, $p2=null, $p3=null){ // Failure response
		$r = array('success' => FALSE);
		if(is_array($p1)){ //Means that p1 is ERRORS
			$tmp = array();
    		foreach($p1 as $id => $msg) $tmp[] = array('id' => $id, 'msg' => $msg);
			$r['errors'] = $tmp;
			if(is_string($p2)) {
				$r['msg'] = $p2;//Means that p2 is MSG
				if(is_array($p3)) $r = array_merge($r, p3); // The p3 parameter is DATA that will be merge with the response
			}elseif(is_array($p2)) $r = array_merge($r, $p2); //Means that p2 is more data for merge with the response
		}elseif(is_string($p1)){//Means that p1 is MSG
			$r['msg'] = $p1;
			if (is_array($p2)) $r = array_merge($r, $p2);//The p2 parameter is DATA that will be merge with the response
		}
		return json_encode($r);
	}

	

	public function tresponse($data){//Tree Response
		return json_encode($data);
	}

	

	/**
      *  Determina que sólo un usuario logueado podrá acceder a esta.
      *  Only for puede ser un user o un client, si está vacio, será para ambos
      */
      public function isPrivate( ){
        if (!$this->ion_auth->logged_in()){
            redirect('auth/login', 'refresh');
        }else{
            $module = strtolower( get_class( $this ) );
            if ( !$this->ion_auth->have_module($module) ){
                $this->data["message"] = "No tienes los permisos suficientes";
                if( IS_AJAX )
                    echo $this->fresponse("Sin permisos");
                else
                    echo $this->aview("", array(), TRUE);   
                exit();
            } 
        }
      }

	
  /** Abrirá una función en un ámbito (front|end) */
    public function load_view($template = "", $subview=NULL, $data=array()){
        $this->data["tag_title"] = $this->_getTitle();
        
         $this->data["js"] = array_merge( 
            $this->data["js"]["base"],
            $this->data["js"]["plugs"],
            $this->data["js"]["globals"],
            $this->data["js"]["page"]
        );
        
        $this->data["css"] = array_merge( 
            $this->data["css"]["base"],
            $this->data["css"]["plugs"],
            $this->data["css"]["globals"],
            $this->data["css"]["page"]
        );
        
        $this->data["cls_section"] = "" . $this->uri->rsegment(1);
        $this->data["cls_subsection"] = $this->uri->rsegment(1) . "-" . $this->uri->rsegment(1) ? $this->uri->rsegment(1) . "-" . $this->uri->rsegment(2) : ""; 
        
        $data['subview'] = $subview;
        
        $new_data = array_merge($this->data, $data);

        $this->parser->parse($template, $new_data);
    }

    
    /** Abre una vista dada a partir de la carpeta view y la carga en el template
     * @param $subview
     * */
    public function view($subview=NULL, $data=array()  ){  //Muestra la página de frontend, NO LA DE ADMINISTRADOR
        $this->load_view("tpl/page.tpl", $subview, $data);        
        //$this->load->view( "tpl/page.php", $new_data );
    }


    /** Muestra la página del administrador, NO LA DEL FRONTEND
     *
     */
    public function aview($subview='', $data=array()  ){
        $this->parser->set_theme("admin");  
        $this->load_view("admin/tpl/page.tpl", "admin/" . $subview, $data);        
    }       

    
	

	

	/* Genera un paginador**/

	public function getPag($conf_data = array() ){
		$base_data = array(
			'base_url'  => "/", // la url a la que apuntará base_url() . "/propiedad/listado",
			'total_rows' => 10 ,
			'per_page' => 5,
			'num_links' => 8,
			"suffix" => "",
			'last_link' => '>>',
			'first_link' => '<<'
		);

		$data = array_merge($base_data, $conf_data);
		$this->load->library('pagination');
		$this->pagination->initialize($data);
		return $this->pagination->create_links();			
	}

	

}
