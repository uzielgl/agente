<?php
class MY_Model extends CI_Model{
/*	var $tb = ""; //tabla
	var $pre = ""; //prefix
	var $pk = "";
	*/
	
	public function __construct(){
		parent::__construct();
						
		//Creamos el paginador si es que hay 
	}
	
	public function setLimit($limit, $offset = 0){
		$this->limit = $limit;
		$this->offset = $offset;
		$this->db->limit($limit, $offset);
	}
	
	public function get(){
		$rs = $this->db->get($this->tb);
		return $rs->result_array();
	}
	
	public function getById($id){
		$this->db->where_in("{$this->pk}", $id);
		$rs = $this->get();
		if( count($rs) > 0 )
			return $rs[0];
		else
			return false;
	}
	
	
	public function count_all_results(){
		$this->db->from($this->tb);
		$rs = $this->db->count_all_results();
		return $rs;
	}
	
	public function getCount(){
		$rs = $this->db->query($this->count_all_query);
		print_r($rs->row_array());
	}
	
	public function getPag( $base_config = array() ){
		$default_config = array(
			"base_url" => base_url(),
			"total_rows" => $this->total,
			"per_page" => $this->limit,
			"num_links" => 6,
			'last_link' => '>>',
			'first_link' => '<<'
//			$config["suffix"] = "#socios";
		);
		
		$config = array_merge( $default_config, $base_config );
		
		$this->load->library('pagination');
		$this->pagination->initialize($config);
		return $this->pagination->create_links();		
	}
	
	public function getTotal(){ //Ejecutar primero esta
		$this->total = $this->count_all_results();
		return $this->total;
	}
	
	public function upd($id, $data= array() ){
		$this->db->where($this->pk, $id);
		$this->db->update($this->tb, $data);
		return TRUE;
	}
	
	public function add(&$data, $returnAllData = FALSE){
		try{
			$tmp = explode(" ", $this->tb);
			if( count($tmp) == 2) list($tb, $alias) = $tmp;
			else {
				$tb = $this->tb;
				$alias = $this->tb;
			}
		}catch(Exception $e){
			$tb = $this->tb;
			$alias = $this->tb;
		}
		
		
		$this->db->insert($tb, $data);
		$data[$this->pk] = $this->db->insert_id();
		if( $returnAllData )
			return $this->getById( $this->db->insert_id() );
		return true;
	}
	
	public function del($id){
		try{
			$tmp = explode(" ", $this->tb);
			if( count($tmp) == 2) list($tb, $alias) = $tmp;
			else {
				$tb = $this->tb;
				$alias = $this->tb;
			}
		}catch(Exception $e){
			$tb = $this->tb;
			$alias = $this->tb;
		}
		
		$this->db->where_in($this->pk, $id);
		$this->db->delete($tb); 
	}

	/*Se utiliza cuando el select tiene SQL_CALC_FOUND_ROWS*/
	public function foundRows()	{
		$rs = $this->db->query("select FOUND_ROWS() as total")->row_array();
		return $rs["total"];		
	}
}
