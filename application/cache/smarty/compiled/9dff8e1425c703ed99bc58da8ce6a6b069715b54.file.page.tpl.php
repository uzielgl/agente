<?php /* Smarty version Smarty-3.1.12, created on 2013-04-11 11:43:23
         compiled from "application\views\tpl\page.tpl" */ ?>
<?php /*%%SmartyHeaderCode:3276750ff1db23fc984-81887259%%*/if(!defined('SMARTY_DIR')) exit('no direct access allowed');
$_valid = $_smarty_tpl->decodeProperties(array (
  'file_dependency' => 
  array (
    '9dff8e1425c703ed99bc58da8ce6a6b069715b54' => 
    array (
      0 => 'application\\views\\tpl\\page.tpl',
      1 => 1365693426,
      2 => 'file',
    ),
  ),
  'nocache_hash' => '3276750ff1db23fc984-81887259',
  'function' => 
  array (
  ),
  'version' => 'Smarty-3.1.12',
  'unifunc' => 'content_50ff1db247d121_15306941',
  'variables' => 
  array (
    'tag_title' => 0,
    'css' => 0,
    'i' => 0,
    'cls_section' => 0,
    'cls_subsection' => 0,
    'subview' => 0,
    'content' => 0,
    'js' => 0,
  ),
  'has_nocache_code' => false,
),false); /*/%%SmartyHeaderCode%%*/?>
<?php if ($_valid && !is_callable('content_50ff1db247d121_15306941')) {function content_50ff1db247d121_15306941($_smarty_tpl) {?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" href="<?php echo base_url();?>
images/favicon.gif" type="image/gif" />
		<script type="text/javascript">
			//Global variables
			var b_url = "<?php echo base_url();?>
";
			var s_url = "<?php echo site_url();?>
"; //site_url
		</script>
		<title><?php echo $_smarty_tpl->tpl_vars['tag_title']->value;?>
</title>
		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['css']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
			<?php echo css($_smarty_tpl->tpl_vars['i']->value);?>

		<?php } ?>
		<!--[if lt IE 9]><?php echo css("ielt9.css");?>
<![endif]-->
	</head>
	<body class="<?php echo $_smarty_tpl->tpl_vars['cls_section']->value;?>
 <?php echo $_smarty_tpl->tpl_vars['cls_subsection']->value;?>
 ">
   		<div id="wrapper">
   				
   				<div class="clearfix"></div>
   				
				<?php if (isset($_smarty_tpl->tpl_vars['subview']->value)){?>
					<?php echo $_smarty_tpl->getSubTemplate (((string)$_smarty_tpl->tpl_vars['subview']->value).".tpl", $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, null, null, array(), 0);?>

				<?php }?>
				
				<?php if (isset($_smarty_tpl->tpl_vars['content']->value)){?>
					<?php echo $_smarty_tpl->tpl_vars['content']->value;?>

				<?php }?>
				
   		</div> <!-- /wrapper --> 
   		<?php  $_smarty_tpl->tpl_vars['i'] = new Smarty_Variable; $_smarty_tpl->tpl_vars['i']->_loop = false;
 $_from = $_smarty_tpl->tpl_vars['js']->value; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array');}
foreach ($_from as $_smarty_tpl->tpl_vars['i']->key => $_smarty_tpl->tpl_vars['i']->value){
$_smarty_tpl->tpl_vars['i']->_loop = true;
?>
			<?php echo js($_smarty_tpl->tpl_vars['i']->value);?>

		<?php } ?>
	</body>
</html>
	



<?php }} ?>