<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		<link rel="icon" href="{base_url()}images/favicon.gif" type="image/gif" />
		<script type="text/javascript">
			//Global variables
			var b_url = "{base_url()}";
			var s_url = "{site_url()}"; //site_url
		</script>
		<title>{$tag_title}</title>
		{foreach $css as $i}
			{css( $i )}
		{/foreach}
		<!--[if lt IE 9]>{css( "ielt9.css")}<![endif]-->
	</head>
	<body class="{$cls_section} {$cls_subsection} ">
   		<div id="wrapper">
   				
   				<div class="clearfix"></div>
   				
				{if isset($subview) }
					{include file="$subview.tpl"}
				{/if}
				
				{if isset($content)}
					{$content}
				{/if}
				
   		</div> <!-- /wrapper --> 
   		{foreach $js as $i}
			{js( $i )}
		{/foreach}
	</body>
</html>
	



