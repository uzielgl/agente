    <div id="footer">
       	<div class="wrapper">
          	<div class="on-5 columns">
             	<div class="column">
                	<a href="{site_url("contacto")}">Contacto</a>
                </div>
                <div class="column">
                	<a href="{site_url("cms/admin/terminos_condiciones")}">Términos y condiciones</a>
                </div>
                <div class="column">
                	Powered by: <a class="color-1" href="http://www.casasyterrenos.com/" target="_blank"><strong>casasyterrenos.com</strong></a>
                </div>
                <div class="column">
                	<a href="{site_url("cms/admin/politicas_de_privacidad")}">Política de privacidad</a>
                </div>
                <div class="column">
                	<a href="{site_url("cms/admin/mapa_del_sitio")}">Mapa del sitio</a>
                </div>
             </div>
          </div>
     </div>