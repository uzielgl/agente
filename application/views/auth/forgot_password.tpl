<div class="container">
	<div class="row">
	    <div class="span offset2">
	      <div class="login">
	        <form method="post" action="{site_url("auth/forgot_password")}" class="form-horizontal">
	          <div class="control-group">
	            <div class="controls">
	              <h4>Recuperar contraseña</h4>
	              <span>Por favor introduzca su email y le enviaremos un correo para reestablecer su contraseña</span>
	            </div>
	          </div>
	          <div class="control-group {cls_error("email")}">
	            <label class="control-label" for="email">{$identity_label}  </label>
	            <div class="controls">
					
					{form_input($email)}
					{help_inline( validation_errors() )}
	            </div>
	          </div>
	          
	        
	          <div class="control-group"> 
	            <div class="controls">
	            	<button class="btn btn-info" type="submit"><i class="icon-play icon-white"> </i> Recordar contraseña</button>
				</div>
	          </div>
	          
	          {if !validation_errors() && $message}
	          <div class="control-group error">
	            <div class="controls">
	              {help_inline($message)}
	            </div>
	          </div>
	          {/if}
	  
	        </form>
	        
	        
	        
	      </div>
	    </div>
  </div>
  
</div>