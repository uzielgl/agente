<div class="container">
	<div class="row">
	    <div class="span offset2">
	      <div class="login">
	        <form method="post" action="{site_url("auth/login")}" class="form-horizontal">
	          <div class="control-group">
	            <div class="controls">
	              <h4>Login</h4>
	            </div>
	          </div>
	          <div class="control-group {cls_error("identity")}">
	            <label class="control-label" for="identity">Email </label>
	            <div class="controls">
					{form_input($identity)}
					{help_inline( form_error("identity") )}
	            </div>
	          </div>
	          <div class="control-group {cls_error("password")}">
	            <label class="control-label" for="password">Contraseña </label>
	            <div class="controls">
	              {form_input($password)}
	              {help_inline( form_error("password") )}
	            </div>
	          </div>
	          
	          <div class="control-group">
	            <div class="controls ">
	              <label class="checkbox">
					{form_checkbox('remember', '1', set_value("remember"), 'id="remember" ')}
	              	Recordarme
	              </label>
	            </div>
	          </div>
	          
	          <div class="control-group"> 
	            <div class="controls">
	            	<button class="btn btn-info"><i class="icon-play icon-white"> </i> Entrar</button>
				</div>
	          </div>
	          
	          {if !validation_errors() && $message}
	          <div class="control-group error">
	            <div class="controls">
	              {help_inline($message)}
	            </div>
	          </div>
	          {/if}
	          
	          <div class="control-group"> 
	            <div class="controls">
	            	<a href="{site_url('auth/forgot_password')}" class="btn btn-warning"><i class="icon-repeat icon-white"> </i> Recuperar contraseña</a>
				</div>
	          </div>
	          
	        </form>
	        
	        
	        
	      </div>
	    </div>
  </div>
  
</div>