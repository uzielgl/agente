


tablero = new Tablero("tablero", [
	[ new Coordenada(0,0), new Coordenada(4,10) ],
	[ new Coordenada(5,0), new Coordenada(7,8) ],
	[ new Coordenada(8,0), new Coordenada(10,10) ],
	[ new Coordenada(11,4), new Coordenada(13,6) ] 
]);


//Ponemos los objetos
tablero.put( new Objeto(),  2, 4);
tablero.put( new Objeto(),  2, 5);
tablero.put( new Objeto(),  2, 6);
tablero.put( new Objeto(),  3, 4);
tablero.put( new Objeto(),  3, 5);
tablero.put( new Objeto(),  3, 6);
tablero.put( new Objeto(),  4, 4);
tablero.put( new Objeto(),  5, 4);
tablero.put( new Objeto(),  6, 4);
tablero.put( new Objeto(),  6, 5);
tablero.put( new Objeto(),  6, 6);
tablero.put( new Objeto(),  7, 4);
tablero.put( new Objeto(),  7, 5);
tablero.put( new Objeto(),  7, 6);


//Ponemos el agente
agente = new Agente();
var casillaRand = tablero.getRandFreeCasilla();

tablero.put( agente, casillaRand.coordenada.x, casillaRand.coordenada.y );


