var Tablero = new Class({
	casillas : [],
	
	initialize : function( id_div_container, coordenadas ){
		this.$el = jQuery("#" + id_div_container).css({
			position: "relative"
		})
		
		for( var x=0; x < coordenadas.length; x++ ){
			var from = coordenadas[x][0];
			var to = coordenadas[x][1];
			
			//Creamos las posiciones del tablero
			for(var i= from.y ; i<=to.y; i++ ){
				if( !this.casillas[i]) this.casillas[i] = [];
				for( var j= from.x; j<=to.x; j++){
					this.casillas[i][j] = new Casilla( new Coordenada(j, i) );
					this.$el.append( this.casillas[i][j].getHtml() );
				}
			}
		}
		console.log( this.casillas );
	},
	
	put : function( ficha, x, y ){ //Ficha es un agente o tablero
		var casilla = this.getCasilla(x, y);
		ficha.tablero = this;
		
		ficha.$el.css({
			top : casilla.$el.css("top"),
			left : casilla.$el.css("left")
		})
		
		this.$el.append( ficha.$el );
		
		casilla.put( ficha );
		
		if( instanceOf(ficha, Pacman) ){ 
			ficha.updS();
		}
	},
	
	getCasilla : function( coordenada, y ){
		if( instanceOf( coordenada, Coordenada ) ){ 
			if( this.casillas[ coordenada.y  ] )
				if( this.casillas[ coordenada.y  ][coordenada.x ] )
					return this.casillas[ coordenada.y ][ coordenada.x];
				else
					console.warn("No existe algo en la posición y");
			else
				console.warn("No existe algo en la posición x");
		}else{ //Si no, permite el uso de dos enteros, "x" y "y"
			var x = coordenada;
			return this.getCasilla( new Coordenada( x, y) )
		}
	},
	
	getRandFreeCasilla : function(){
		var casilla;
		do{
			casilla = this.casillas.getRandom().getRandom();
		}while( !casilla || casilla.inUse );
		return casilla;
	},
	
	//Regresa las casillas para el algoritmo astar
	getGrid : function(){
		var grid = [];
		for( var i = 0; i < this.casillas.length ; i++ ){
			grid[i] = [];
			for( var j = 0; j < this.casillas[i].length; j++)
				grid[i][j] = this.casillas[i][j].inUse ? GraphNodeType.WALL : GraphNodeType.OPEN ;
		}
		return grid;
	},
	
	getGraph : function(){
		return new Graph( this.getGrid() );
	}
	
	
		
	
});



