
var Pacman = new Class({
	cls : 'pacman',
	Extends : Ficha,
	initialize : function(){
		this.getEl();
		this.$el.addClass( "pacman" );
	},
	s : {},//sensores
	//Actualizará sus Sensores
	updS: function(){ 
		this.s[1] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y - 1 );
		this.s[2] = this.checkCasillaForS( this.casilla.coordenada.x, this.casilla.coordenada.y - 1 );
		this.s[3] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y - 1 );
		this.s[4] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y );
		this.s[5] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y + 1 );
		this.s[6] = this.checkCasillaForS( this.casilla.coordenada.x, this.casilla.coordenada.y + 1 );
		this.s[7] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y + 1);
		this.s[8] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y );
		
	},
	//Checa la casilla y regresa 0 ó 1 para el s
	checkCasillaForS : function( x, y ){
		casilla = this.tablero.getCasilla( x, y );
		if( casilla && !casilla.inUse ) return 0;
		else return 1;
	},
	
	_moveToCasilla: function( casilla ){
		this.parent(casilla); //Este llama al método padre.
		this.updS();
		return 1;	
	},
	
	moverIa : function(){
		//Se aleja del que esté mas cerca
		//Sacamos la distancia manhatta para cada fantasma
		var dis1 = this.manhattan( this.casilla, fan1.casilla );
		var dis2 = this.manhattan( this.casilla, fan2.casilla );
		var dis3 = this.manhattan( this.casilla, fan3.casilla );
		
		var min = Math.min( dis1, dis2, dis3 );
		
		var alejarse_de; // Fantasma del cual hay que alejarse
		if( dis1 == min ) alejarse_de = fan1.casilla;
		if( dis2 == min ) alejarse_de = fan2.casilla;
		if( dis3 == min ) alejarse_de = fan3.casilla;
		console.log( alejarse_de );
				
		//Calculamos la distancia manhatta para todas las posiciones
		//y sacamos el mayor
		var mayor = this.tablero.getCasilla(1,1);
		for( var x = 0; x < this.tablero.casillas.length ; x++){
			if( this.tablero.casillas[x] ){
				for( var y = 0; y < this.tablero.casillas[y].length; y++){
					if( this.tablero.casillas[x][y]){
						if( !this.tablero.casillas[x][y].inUse ){
							if( this.manhattan( alejarse_de, this.tablero.casillas[x][y] )  >  this.manhattan( mayor, alejarse_de ) )
								mayor = this.tablero.casillas[x][y];
						}
					}
				}
			}
		}
		
		
		//Nos movemos hacía el mayor
		var graph = this.tablero.getGraph();
		var start = graph.nodes[this.casilla.coordenada.y][this.casilla.coordenada.x];
		var end = graph.nodes[mayor.coordenada.y][mayor.coordenada.x]; //X son hacía abajo, y Y hacia la derecha
		var result = astar.search(graph.nodes, start, end);
		if ( result[0] )
			this._moveToCasilla( this.tablero.getCasilla( result[0].y, result[0].x ) );
		else 
			return false;
		return true;	
		
		/*
		x1 = this.s[2] || this.s[3];
		x2 = this.s[4] || this.s[5];
		x3 = this.s[6] || this.s[7];
		x4 = this.s[8] || this.s[1];
		
		if( x1 || x2 || x3 || x4 ){
			if( x1 && !x2 ) this._moveToCardinal( Ficha.EAST );
			if( x2 && !x3 ) this._moveToCardinal( Ficha.SOUTH );
			if( x3 && !x4 ) this._moveToCardinal( Ficha.WEST );
			if( x4 && !x1 ) this._moveToCardinal( Ficha.NORTH ); 
		} else{
			this._moveToCardinal( Ficha.NORTH );
		}*/
	},
	
	moveToFicha : function( ficha ){
		//Donde está la ficha ?
		var esta_en = [];
		var distancia_x = Math.abs( ficha.casilla.coordenada.x - this.casilla.coordenada.x );
		var distancia_y = Math.abs( ficha.casilla.coordenada.y - this.casilla.coordenada.y );
		if( ficha.casilla.coordenada.x > this.casilla.coordenada.x  )
			esta_en.push( Ficha.EAST );
		if( ficha.casilla.coordenada.x < this.casilla.coordenada.x  )
			esta_en.push( Ficha.WEST );
		if( ficha.casilla.coordenada.y > this.casilla.coordenada.y  )
			esta_en.push( Ficha.SOUTH );
		if( ficha.casilla.coordenada.y < this.casilla.coordenada.y  )
			esta_en.push( Ficha.NORTH );
		console.log( distancia_x);
		console.log( distancia_y);
		if( esta_en.length != 0)
			if( distancia_x > 1 || distancia_y > 1){
				if( !this._moveToCardinal( esta_en.getRandom() ) ){
					this.moverIa();
				} 
			}else{
				console.log("en else");
			}	
		
	}
		
});
