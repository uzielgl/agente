
var Fantasma = new Class({
	cls : 'fantasma',
	Extends : Ficha,
	initialize : function( cls, fn_mover_ia ){
		cls = cls || "fantasma";
		fn_mover_ia = fn_mover_ia || this.moverIa;
		this.moverIa = fn_mover_ia;
		
		this.getEl();
		this.$el.addClass( cls );
	},
	s : {},//sensores
	//Actualizará sus Sensores
	updS: function(){ 
		this.s[1] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y - 1 );
		this.s[2] = this.checkCasillaForS( this.casilla.coordenada.x, this.casilla.coordenada.y - 1 );
		this.s[3] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y - 1 );
		this.s[4] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y );
		this.s[5] = this.checkCasillaForS( this.casilla.coordenada.x + 1, this.casilla.coordenada.y + 1 );
		this.s[6] = this.checkCasillaForS( this.casilla.coordenada.x, this.casilla.coordenada.y + 1 );
		this.s[7] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y + 1);
		this.s[8] = this.checkCasillaForS( this.casilla.coordenada.x - 1, this.casilla.coordenada.y );
	},
	//Checa la casilla y regresa 0 ó 1 para el s
	checkCasillaForS : function( x, y ){
		casilla = this.tablero.getCasilla( x, y );
		if( casilla && !casilla.inUse ) return 0;
		else return 1;
	},
	
	moverIa : function(){
		//Obtenemos las casillas al rededor del pacman
		var cas_arriba = this.tablero.getCasilla( pacman.casilla.coordenada.x, pacman.casilla.coordenada.y - 1 );
		var cas_abajo = this.tablero.getCasilla( pacman.casilla.coordenada.x, pacman.casilla.coordenada.y + 1 );
		var cas_izquierda = this.tablero.getCasilla( pacman.casilla.coordenada.x - 1, pacman.casilla.coordenada.y );
		var cas_derecha = this.tablero.getCasilla( pacman.casilla.coordenada.x + 1, pacman.casilla.coordenada.y );
		
		//Filtramos sólo las que no estén en uso
		var direcciones = {};
		if( !cas_arriba.inUse ) direcciones.arriba = cas_arriba;
		if( !cas_abajo.inUse ) direcciones.abajo = cas_abajo;
		if( !cas_izquierda.inUse ) direcciones.izquierda = cas_izquierda;
		if( !cas_derecha.inUse ) direcciones.derecha = cas_derecha;
		/*
		if( !cas_arriba.inUse || ( cas_arriba.x == this.casilla.x && cas_arriba.y == this.casilla.y) ) direcciones.arriba = cas_arriba;
		if( !cas_abajo.inUse || ( cas_abajo.x == this.casilla.x && cas_abajo.y == this.casilla.y) ) direcciones.abajo = cas_abajo;
		if( !cas_izquierda.inUse || ( cas_izquierda.x == this.casilla.x && cas_izquierda.y == this.casilla.y) ) direcciones.izquierda = cas_izquierda;
		if( !cas_derecha.inUse || ( cas_derecha.x == this.casilla.x && cas_derecha.y == this.casilla.y) ) direcciones.derecha = cas_derecha;
		*/
		
		/*
		if( !cas_arriba.inUse ) direcciones.cas_arriba = this.manhattan( this.casilla, cas_arriba );
		if( !cas_abajo.inUse ) direcciones.cas_abajo = this.manhattan( this.casilla, cas_abajo);
		if( !cas_izquierda.inUse ) direcciones.cas_izquierda = this.manhattan( this.casilla, cas_izquierda);
		if( !cas_derecha.inUse ) direcciones.cas_derecha = this.manhattan( this.casilla, cas_derecha);
		*/
		
		//Sacamos la menor
		var distancias = [];
		for( var x in direcciones){
			distancias.push( this.manhattan( direcciones[x], this.casilla ) ); 
		}
		//console.log(direcciones);
		//Sacamos la menor distancia
		var min_dis = Math.min.apply( null, distancias);
		//Recorremos nuevamente para sacar hacia donde va a ir
		if ( this.manhattan( pacman.casilla, this.casilla) == 1 ) return false;
		
		var to; 
		for( var x in direcciones){
			if ( this.manhattan( direcciones[x], this.casilla ) == min_dis){
				to = direcciones[x];
				break;
			}
		}
		if ( !to ) return false;
		
		
		var graph = tablero.getGraph();
		var start = graph.nodes[this.casilla.coordenada.y][this.casilla.coordenada.x];
		var end = graph.nodes[to.coordenada.y][to.coordenada.x]; //X son hacía abajo, y Y hacia la derecha
		var result = astar.search(graph.nodes, start, end);
		if ( result[0] )
			this._moveToCasilla( this.tablero.getCasilla( result[0].y, result[0].x ) );
		else 
			return false;
		return true;	
		/*
		var graphWithWeight = this.tablero.getGraph();
		var startWithWeight = graphWithWeight.nodes[this.casilla.coordenada.y][this.casilla.coordenada.x];
		var endWithWeight = graphWithWeight.nodes[pacman.casilla.coordenada.y][pacman.casilla.coordenada.x];
		var r = astar.search(graphWithWeight.nodes, startWithWeight, endWithWeight);
		console.log(r);
		this._moveToCasilla( this.tablero.getCasilla( r[0].x, r[0].y ) );*/
	},
	
	moveToFicha : function( ficha ){
		//Donde está la ficha ?
		var esta_en = [];
		var distancia_x = Math.abs( ficha.casilla.coordenada.x - this.casilla.coordenada.x );
		var distancia_y = Math.abs( ficha.casilla.coordenada.y - this.casilla.coordenada.y );
		if( ficha.casilla.coordenada.x > this.casilla.coordenada.x  )
			esta_en.push( Ficha.EAST );
		if( ficha.casilla.coordenada.x < this.casilla.coordenada.x  )
			esta_en.push( Ficha.WEST );
		if( ficha.casilla.coordenada.y > this.casilla.coordenada.y  )
			esta_en.push( Ficha.SOUTH );
		if( ficha.casilla.coordenada.y < this.casilla.coordenada.y  )
			esta_en.push( Ficha.NORTH );
		console.log( distancia_x);
		console.log( distancia_y);
		if( esta_en.length != 0)
			if( distancia_x > 1 || distancia_y > 1){
				if( !this._moveToCardinal( esta_en.getRandom() ) ){
					this.moverIa();
				} 
			}else{
				console.log("en else");
			}	
		
	}
		
});
