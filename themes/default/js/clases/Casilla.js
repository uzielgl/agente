var Casilla = new Class({
	coordenada : null,
	width : 32,
	height : 32,
	
	inUse : false,
	
	initialize : function( coordenada){
		this.coordenada = coordenada;
	},
	
	getHtml : function(){
		this.$el = jQuery("<div class='casilla'>"+ this.coordenada.x + ", " + this.coordenada.y  +"</div>");
		this.$el.css({
			width : this.width,
			height: this.height,
			border : "solid 1px #000",
			position: "absolute",
			top : this.coordenada.y * this.width,
			left : this.coordenada.x * this.width
		});
		return this.$el;
	},
	
	put : function( ficha ){ 
		this.inUse = true;
		ficha.casilla = this;
	},
	
	takeOut : function(){
		this.inUse = false;
	}
	
});
