var Ficha = new Class({
	
	width: 32,
	height: 32,
	
	casilla : null, //Casilla en la que se encuentra el agente
	tablero : null, // Tablero en el que está el agente y las casillas 
	
	onlyApper : false, 
	
	getEl : function(){
		this.$el = jQuery("<div></div>");
		this.$el.css({
			width : this.width,
			height: this.height,
			position: "absolute"
		});
		return this.$el;
	},
	
	_moveToCasilla : function( casilla ){ 
		if( this.onlyApper ){ 
			var me = this;
			this.onlyApper = false;
			this.$el.fadeOut(function(){
				me.$el.css({
					top: casilla.$el.css("top"),
					left: casilla.$el.css("left")
				})
				.fadeIn();
			})
		}else{
			this.$el.animate({
				duration : 100,
				top: casilla.$el.css("top"),
				left: casilla.$el.css("left")
			});
		}
		
		this.casilla.inUse = false;
		casilla.put( this );
		return 1;
		//Debemos de actualizar sus S's en el hijo 
	},
	
	_moveToCardinal : function( cardinal_point ){
		if( cardinal_point == Ficha.NORTH ){
			var casilla_to_check = tablero.getCasilla( this.casilla.coordenada.x, this.casilla.coordenada.y - 1); 
			if( casilla_to_check && !casilla_to_check.inUse ){
				return this._moveToCasilla( casilla_to_check );
			}else
				console.warn("La casilla no existe o está en uso")
		}
		
		if( cardinal_point == Ficha.EAST ){
			var to_x = this.casilla.coordenada.x + 1;
			var to_y = this.casilla.coordenada.y;
			
			if( this.casilla.coordenada.x == tablero.casillas[ this.casilla.coordenada.y ].length - 1 ){ //Significa que está en la frontera y tiene que ir al pasillo mágico
				var to_x = 0;
				this.onlyApper = true;
			}
			
			
			var casilla_to_check = tablero.getCasilla(to_x, to_y); 
			if( casilla_to_check && !casilla_to_check.inUse ){
				return this._moveToCasilla( casilla_to_check );
			}else
				console.warn("La casilla no existe o está en uso")
		}
		
		if( cardinal_point == Ficha.SOUTH ){
			var casilla_to_check = tablero.getCasilla( this.casilla.coordenada.x, this.casilla.coordenada.y + 1); 
			if( casilla_to_check && !casilla_to_check.inUse ){
				return this._moveToCasilla( casilla_to_check );
			}else
				console.warn("La casilla no existe o está en uso")
		}
		
		if( cardinal_point == Ficha.WEST ){
			
			var to_x = this.casilla.coordenada.x -1;
			var to_y = this.casilla.coordenada.y;
			
			if( this.casilla.coordenada.x == 0 ){ //Significa que está en la frontera y tiene que ir al pasillo mágico
				var to_x = tablero.casillas[ this.casilla.coordenada.y ].length - 1;
				this.onlyApper = true;
			}
			var casilla_to_check = tablero.getCasilla( to_x, to_y ); 
			
			if( casilla_to_check && !casilla_to_check.inUse ){
				return this._moveToCasilla( casilla_to_check );
			}else
				console.warn("La casilla no existe o está en uso")
		}
		return 0;
		
	},
	manhattan: function(cas0, cas1) {
        // See list of heuristics: http://theory.stanford.edu/~amitp/GameProgramming/Heuristics.html
        var d1 = Math.abs (cas1.coordenada.x - cas0.coordenada.x);
        var d2 = Math.abs (cas1.coordenada.y - cas0.coordenada.y);
        /*
		if( pos1.x == 9){ //Implementación del pasillo mágico
			if( pos0.y > 9  ){
				console.log(pos0.x);
				var dm = 18 - pos0.y + pos1.y
				console.log( dm );
				d1 = Math.min( d1, dm );
			}		
		}*/
        return d1 + d2;
    },
	
}).extend({
	NORTH : 1,
	EAST : 2, //right
	SOUTH : 3,
	WEST : 4 //left
});
