
tablero = new Tablero("tablero", [
	[ new Coordenada(0,0), new Coordenada(18,20) ]
]);

/* Para las paredes y obstaculos */
//Contorno izquierdo
tablero.put( new Objeto(),  0, 0);
tablero.put( new Objeto(),  0, 1);
tablero.put( new Objeto(),  0, 2);
tablero.put( new Objeto(),  0, 3);
tablero.put( new Objeto(),  0, 4);
tablero.put( new Objeto(),  0, 5);
tablero.put( new Objeto(),  0, 6);
tablero.put( new Objeto(),  0, 7);
tablero.put( new Objeto(),  0, 8);
//tablero.put( new Objeto(),  0, 9);
tablero.put( new Objeto(),  0, 10);
tablero.put( new Objeto(),  0, 11);
tablero.put( new Objeto(),  0, 12);
tablero.put( new Objeto(),  0, 13);
tablero.put( new Objeto(),  0, 14);
tablero.put( new Objeto(),  0, 15);
tablero.put( new Objeto(),  0, 16);
tablero.put( new Objeto(),  0, 17);
tablero.put( new Objeto(),  0, 18);
tablero.put( new Objeto(),  0, 19);
tablero.put( new Objeto(),  0, 20);

//Contorno derecho
tablero.put( new Objeto(),  18, 0);
tablero.put( new Objeto(),  18, 1);
tablero.put( new Objeto(),  18, 2);
tablero.put( new Objeto(),  18, 3);
tablero.put( new Objeto(),  18, 4);
tablero.put( new Objeto(),  18, 5);
tablero.put( new Objeto(),  18, 6);
tablero.put( new Objeto(),  18, 7);
tablero.put( new Objeto(),  18, 8);
//tablero.put( new Objeto(),  18, 9);
tablero.put( new Objeto(),  18, 10);
tablero.put( new Objeto(),  18, 11);
tablero.put( new Objeto(),  18, 12);
tablero.put( new Objeto(),  18, 13);
tablero.put( new Objeto(),  18, 14);
tablero.put( new Objeto(),  18, 15);
tablero.put( new Objeto(),  18, 16);
tablero.put( new Objeto(),  18, 17);
tablero.put( new Objeto(),  18, 18);
tablero.put( new Objeto(),  18, 19);
tablero.put( new Objeto(),  18, 20);

//Contorno superior
tablero.put( new Objeto(),  1, 0);
tablero.put( new Objeto(),  2, 0);
tablero.put( new Objeto(),  3, 0);
tablero.put( new Objeto(),  4, 0);
tablero.put( new Objeto(),  5, 0);
tablero.put( new Objeto(),  6, 0);
tablero.put( new Objeto(),  7, 0);
tablero.put( new Objeto(),  8, 0);
tablero.put( new Objeto(),  9, 0);
tablero.put( new Objeto(),  10, 0);
tablero.put( new Objeto(), 	11, 0);
tablero.put( new Objeto(),  12, 0);
tablero.put( new Objeto(),  13, 0);
tablero.put( new Objeto(),  14, 0);
tablero.put( new Objeto(),  15, 0);
tablero.put( new Objeto(),  16, 0);
tablero.put( new Objeto(),  17, 0);

//Contorno inferior
tablero.put( new Objeto(),  1, 20);
tablero.put( new Objeto(),  2, 20);
tablero.put( new Objeto(),  3, 20);
tablero.put( new Objeto(),  4, 20);
tablero.put( new Objeto(),  5, 20);
tablero.put( new Objeto(),  6, 20);
tablero.put( new Objeto(),  7, 20);
tablero.put( new Objeto(),  8, 20);
tablero.put( new Objeto(),  9, 20);
tablero.put( new Objeto(),  10, 20);
tablero.put( new Objeto(), 	11, 20);
tablero.put( new Objeto(),  12, 20);
tablero.put( new Objeto(),  13, 20);
tablero.put( new Objeto(),  14, 20);
tablero.put( new Objeto(),  15, 20);
tablero.put( new Objeto(),  16, 20);
tablero.put( new Objeto(),  17, 20);


//Objeto Arriba el piquito
tablero.put( new Objeto(),  9,1);
tablero.put( new Objeto(),  9,2);
tablero.put( new Objeto(),  9,3);

//Objeto "T" enmedio Derecha
tablero.put( new Objeto(), 5, 4)
tablero.put( new Objeto(), 5, 5)
tablero.put( new Objeto(), 5, 6)
tablero.put( new Objeto(), 5, 7)
tablero.put( new Objeto(), 5, 8)
tablero.put( new Objeto(), 6, 6)
tablero.put( new Objeto(), 7, 6)

//Objetos dentro del escenario
tablero.put( new Objeto(), 11, 6);
tablero.put( new Objeto(), 12, 6);
tablero.put( new Objeto(), 13, 6);
tablero.put( new Objeto(), 13, 5);
tablero.put( new Objeto(), 13, 4);
tablero.put( new Objeto(), 13, 7);
tablero.put( new Objeto(), 13, 8);
tablero.put( new Objeto(), 1, 6);
tablero.put( new Objeto(), 2, 6);
tablero.put( new Objeto(), 3, 6);
tablero.put( new Objeto(), 1, 7);
tablero.put( new Objeto(), 2, 7);
tablero.put( new Objeto(), 3, 7);
tablero.put( new Objeto(), 1, 8);
tablero.put( new Objeto(), 2, 8);
tablero.put( new Objeto(), 3, 8);
tablero.put( new Objeto(), 1, 10);
tablero.put( new Objeto(), 2, 10);
tablero.put( new Objeto(), 3, 10);
tablero.put( new Objeto(), 1, 11);
tablero.put( new Objeto(), 2, 11);
tablero.put( new Objeto(), 3, 11);
tablero.put( new Objeto(), 1, 12);
tablero.put( new Objeto(), 2, 12);
tablero.put( new Objeto(), 3, 12);
tablero.put( new Objeto(), 17, 6);
tablero.put( new Objeto(), 16, 6);
tablero.put( new Objeto(), 15, 6);
tablero.put( new Objeto(), 17, 7);
tablero.put( new Objeto(), 16, 7);
tablero.put( new Objeto(), 15, 7);
tablero.put( new Objeto(), 17, 8);
tablero.put( new Objeto(), 16, 8);
tablero.put( new Objeto(), 15, 8);
tablero.put( new Objeto(), 17, 10);
tablero.put( new Objeto(), 16, 10);
tablero.put( new Objeto(), 15, 10);
tablero.put( new Objeto(), 17, 11);
tablero.put( new Objeto(), 16, 11);
tablero.put( new Objeto(), 15, 11);
tablero.put( new Objeto(), 17, 12);
tablero.put( new Objeto(), 16, 12);
tablero.put( new Objeto(), 15, 12);
tablero.put( new Objeto(), 5, 10);
tablero.put( new Objeto(), 5, 11);
tablero.put( new Objeto(), 5, 12);
tablero.put( new Objeto(), 13, 10);
tablero.put( new Objeto(), 13, 11);
tablero.put( new Objeto(), 13, 12);
tablero.put( new Objeto(), 7, 12);
tablero.put( new Objeto(), 8, 12);
tablero.put( new Objeto(), 9, 12);
tablero.put( new Objeto(), 10, 12);
tablero.put( new Objeto(), 11, 12);
tablero.put( new Objeto(), 9, 13);
tablero.put( new Objeto(), 9, 14);
tablero.put( new Objeto(), 1, 16);
tablero.put( new Objeto(), 17, 16);
tablero.put( new Objeto(), 13, 16);
tablero.put( new Objeto(), 13, 17);
tablero.put( new Objeto(), 13, 18);
tablero.put( new Objeto(), 12, 18);
tablero.put( new Objeto(), 11, 18);
tablero.put( new Objeto(), 14, 18);
tablero.put( new Objeto(), 15, 18);
tablero.put( new Objeto(), 16, 18);

//Ponemos a pacman
pacman = new Pacman();

var casillaRand1 = tablero.getRandFreeCasilla();
tablero.put( pacman, casillaRand1.coordenada.x, casillaRand1.coordenada.y );
//tablero.put( pacman, 0, 9 );

//Ponemos a los fantasmas y sus comportamientos
var casillaRand = tablero.getRandFreeCasilla();
var fan1 = new Fantasma('fan1');
tablero.put( fan1, casillaRand.coordenada.x, casillaRand.coordenada.y );
//tablero.put( fan1, 15, 9);



var casillaRand = tablero.getRandFreeCasilla();
var fan2 = new Fantasma('fan2');
tablero.put( fan2, casillaRand.coordenada.x, casillaRand.coordenada.y );

var casillaRand = tablero.getRandFreeCasilla();
var fan3 = new Fantasma('fan3');
tablero.put( fan3, casillaRand.coordenada.x, casillaRand.coordenada.y );


setInterval(function(){
	fan1.moverIa();
	fan2.moverIa();
	fan3.moverIa();
	pacman.moverIa();
}, 10);

